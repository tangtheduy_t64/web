<!DOCTYPE html>
<html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="script.js"></script>
<link rel="stylesheet" type="text/css" href="trang3.css" />
<body>
<?php
  session_start(); 
  $scores = 0;
  $answer = [];
  for ($i = 0;$i<10;$i++){
    $cookie = isset($_COOKIE[$i]) ? $_COOKIE[$i] : '';
    $answer[$i] = $cookie;
    if($_SESSION['questions'][$i]["Correct"] == $cookie){
      $scores++;
    }
  }
?>
</body>
<div class="quiz">
    <div class = "question">
    <h2>Trắc nghiệm PHP</h2>
    <?php
        echo "<p>Bạn đã trả lời đúng $scores/10 câu hỏi</p>";
        if ($scores < 4){
          echo '<b>Bạn quá kém, cần ôn tập thêm</b>';
        }
        else if ($scores >=4 && $scores < 7){
          echo '<b>Cũng bình thường</b>';
        } 
        else {
          echo '<b>Sắp sửa làm được trợ giảng lớp PHP</b>';
        }
    ?>
    </div>
    <?php
        for ($i = 0; $i < 10; $i++) {
        ?>
        <div class = "question">
            <h3><?php echo "Câu hỏi ".($i+1).": ".$_SESSION['questions'][$i]["question"]?></h3>
            <div
            <?php  {if ($_SESSION['questions'][$i]["Correct"]== "AnswerA" ) {                      
                        echo 'class = "true"';
                    }
                    else if($answer[$i] =="AnswerA" and $answer[$i] != $_SESSION['questions'][$i]["Correct"]){
                      echo 'class = "false"';
                    }
            }
            ?>
            >
                <label><input type="radio" id="choice1" name=<?php echo $i?> value="AnswerA"  onclick="return false;"
                <?php  {if ($answer[$i]== "AnswerA") {echo 'checked';}
                    } 
                    ?>
                >
                <?php echo "A. ".$_SESSION['questions'][$i]["AnswerA"]?>
                </label>
            </div>
            <div
            <?php  {if ($_SESSION['questions'][$i]["Correct"]== "AnswerB" ) {                      
                        echo 'style="color:#00ff00;"';
                    }
                    else if($answer[$i] =="AnswerB" and $answer[$i] != $_SESSION['questions'][$i]["Correct"]){
                      echo 'style="color:red;"';
                    }
            }
            ?>
            >
                <label><input type="radio" id="choice2" name=<?php echo $i?> value="AnswerB" onclick="return false;"
                <?php  {if ($answer[$i]== "AnswerB") {echo 'checked';}
                    } 
                    ?>
                >
                <?php echo "B. ".$_SESSION['questions'][$i]["AnswerB"]?>
                </label>
            </div>
            <div
            <?php  {if ($_SESSION['questions'][$i]["Correct"]== "AnswerC" ) {                      
                        echo 'style="color:#00ff00;"';
                    }
                    else if($answer[$i] =="AnswerC" and $answer[$i] != $_SESSION['questions'][$i]["Correct"]){
                      echo 'style="color:red;"';
                    }
            }
            ?>
            >
                <label><input type="radio" id="choice3" name=<?php echo $i?> value="AnswerC" onclick="return false;"
                <?php  {if ($answer[$i]== "AnswerC") {echo 'checked';}
                    } 
                    ?>
                >
                <?php echo "C. ".$_SESSION['questions'][$i]["AnswerC"]?></label>
            </div>
            <div
            <?php  {if ($_SESSION['questions'][$i]["Correct"]== "AnswerD" ) {                      
                        echo 'style="color:#00ff00;"';
                    }
                    else if($answer[$i] =="AnswerD" and $answer[$i] != $_SESSION['questions'][$i]["Correct"]){
                      echo 'style="color:red;"';
                    }
            }
            ?>
            >
                <label><input type="radio" id="choice4" name=<?php echo $i?> value="AnswerD" onclick="return false;"
                <?php  {if ($answer[$i]== "AnswerD") {echo 'checked';}
                    } 
                    ?>
                >
                <?php echo "D. ".$_SESSION['questions'][$i]["AnswerD"]?></label>
            </div> 
        </div>
        <?php
            }
        ?>
    <div class = "buttons">
    <!-- <button type = "button" class="button" onclick="checkAnswer()">Xem đáp án</button> -->
    <a href="trang1.php" class="button1" style="text-decoration:none;">Làm lại</a>
    </div>  
   </div>
</html>