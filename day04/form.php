<!DOCTYPE html>
<html>

<head>
    <title>Đăng ký</title>
    <meta charset="UTF-8">
    <style>
        .form {
            border: solid 2px;
            border-color: #0B59AC;
            margin-top: 80px;
            margin-left: 400px;
            margin-right: 800px;
            padding-left: 60px;
            padding-right: 60px;
            padding-top: 30px;
            padding-bottom: 10px;
        }

        .row {
            display: flex;
            margin-left: 10px;
            margin-right: 10px;
        }

        .row_first {
            display: flex;
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 20px;
        }

        .label {
            width: 25%;
            background-color: #59AC0B;
            border: solid 2px;
            border-color: #0B59AC;
            color: white;
            font-size: 20px;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
            text-align: center;
        }

        .label_gender {

            color: black;
            font-size: 20px;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 15px;
            margin-bottom: 10px;
            text-align: center;
        }

        .column {
            width: 5%;
        }

        .input {
            width: 60%;
            border: solid 2px;
            border-color: #0B59AC;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 10px;
            margin-bottom: 10px;

        }

        .button {
            width: 25%;
            background-color: #59AC0B;
            border: solid 2px;
            border-color: #0B59AC;
            color: white;
            font-size: 20px;
            padding: 10px;
            padding-top: 18px;
            padding-bottom: 18px;
            border-radius: 10px;
            text-align: center;
            margin-left: 220px;
            margin-top: 30px;
            margin-bottom: 70px;
        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin-top: 25px;
        }

        select {
            width: 35%;
            border: solid 2px;
            border-color: #0B59AC;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        option{
            width: 35%;
        }
        .span {
            color: red;

        }
        .alert{
            margin-left: 10px;
            margin-top: 10px;
            color : red;
            font-style: bold;
            font-size: 20px;
        }
        .success{
            margin-left: 10px;
            margin-top: 10px;
            color : green;
            font-style: bold;
            font-size: 20px;
        }
        .birth{
            width: 30.5%;
            border: solid 2px;
            border-color: #0B59AC;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
</head>

<body>
    <div class="form">
        <?php
        $name = "";
        $gender = "";
        $phankhoa = "";
        $birth = "";
        $address = "";
        if (!empty($_POST['submit'])) {
            $check = true;
            $name = isset($_POST['name']) ? $_POST['name'] : '';
            $gender = isset($_POST['gender']) ? $_POST['gender'] : '';
            $phankhoa = isset($_POST['phankhoa']) ? $_POST['phankhoa'] : '';
            $birth = isset($_POST['birth']) ? $_POST['birth'] : '';
            $address = isset($_POST['address']) ? $_POST['address'] : '';
            if (empty($name)){
                echo '<div class ="alert">Hãy nhập tên.</div> ';
                $check =false;
            }
            if (strlen($gender) == 0){ # do empty(0) tra ve true
                echo '<div class ="alert">Hãy chọn giới tính.</div>';
                $check =false;
            }
            if (empty($phankhoa)){
                echo '<div class ="alert">Hãy chọn phân khoa.</div>';  
                $check =false;
            }
            if (empty($birth)){
                echo '<div class ="alert">Hãy nhập ngày sinh.</div>';
                $check =false;
            }
            #html tự validate ngày -> chỉ check null
            if ($check){
                echo '<div class ="success">Đăng ký thành công.</div>'; #clear dữ liệu
                $name = "";
                $gender = "";
                $phankhoa = "";
                $birth = "";
                $address = "";
            }

        }
        ?>
        <form method="post" action="form.php">
            <div class="row_first">
                <div class="label">Họ và tên<span class="span">*</span></div>
                <div class="column"></div>
                <input class="input" type="text" name="name" value="<?php echo $name; ?>">
            </div>
            <div class="row">
                <div class="label">Giới tính<span class="span">*</span></div>
                <div class="column"></div>
                <?php
                $genders = ["Nam", "Nữ"];
                for ($i = 0; $i < count($genders); $i++) {
                ?>
                    <input class="gender" type="radio" name="gender" value="<?php echo $i; ?>" 
                    <?php  {if ($gender== $i) {echo 'checked';}
                    } ?>>
                    <div class="label_gender"><?php echo $genders[$i] ?></div>
                <?php
                }
                ?>

            </div>
            <div class="row">
                <div class="label">Phân khoa<span class="span">*</span></div>
                <div class="column"></div>
                <select name="phankhoa" id="" value="<?php echo $phankhoa; ?>">
                    <?php
                    $select = ["" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu"];
                    foreach ($select as $key => $value) {
                    ?>
                        <option <?php 
                                    if ($phankhoa == $key) {
                                        echo 'selected';
                                    } ?> value="<?php echo $key; ?>"><?php echo $value ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="row">
                <div class="label">Ngày sinh<span class="span">*</span></div>
                <div class="column"></div>
                <input class="birth" data-date-format="dd/mm/yyyy" type="date" name="birth" placeholder="dd/mm/yyyy"
                value = "<?php echo $birth; ?>"
                >
            </div>
            <div class="row">
                <div class="label">Địa chỉ</div>
                <div class="column"></div>
                <input class="input" type="text" name="address" value = "<?php echo $address; ?>">
            </div>
            <input class="button" type="submit" value="Đăng ký" name="submit">
        </form>
    </div>
</body>

</html>