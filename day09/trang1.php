<!DOCTYPE html>
<html>

<head>
    <title>Trắc nghiệm</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="trang1.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="script.js"></script>
</head>
<?php
        session_start();
        $_SESSION['questions'] = [
            ["question" => "Ai là người đầu tiên phát minh ra PHP",
            "AnswerA"   => "James Gosling",
            "AnswerB"   => "Tim Berners-Lee",
            "AnswerC"   => "Todd Fast",
            "AnswerD"   => "Rasmus Lerdorf",
            "Correct"   => "AnswerD"
            ],
            ["question" => "PHP dựa theo syntax của ngôn ngữ nào?",
            "AnswerA"   => "Basic",
            "AnswerB"   => "Pascal",
            "AnswerC"   => "C",
            "AnswerD"   => "VB Script",
            "Correct"   => "AnswerC"
            ],
            ["question" => "Lệnh nào để xuất giá trị của biến x ra trang web",
            "AnswerA"   => "echo x;",
            "AnswerB"   => "print \$x;",
            "AnswerC"   => "echo \$x;",
            "AnswerD"   => "eco \$x;",
            "Correct"   => "AnswerC"
            ],
            ["question" => "Lệnh nào dùng để xuất 1 mảng ra trang web",
            "AnswerA"   => "print_r",
            "AnswerB"   => "out",
            "AnswerC"   => "echo",
            "AnswerD"   => "print",
            "Correct"   => "AnswerA"
            ],
            ["question" => "Cú pháp đúng khi khai báo 1 biến trong PHP",
            "AnswerA"   => "diem=3;",
            "AnswerB"   => "%diem = 9;",
            "AnswerC"   => "\$ diem=5;",
            "AnswerD"   => "\$diem = 2;",
            "Correct"   => "AnswerD"
            ],
            ["question" => "Phép toán không được dùng so sánh trong PHP",
            "AnswerA"   => "===",
            "AnswerB"   => ">=",
            "AnswerC"   => "!=",
            "AnswerD"   => "<=>",
            "Correct"   => "AnswerD"
            ],
            ["question" => "Hàm nào sau đây dùng để khai báo hằng số",
            "AnswerA"   => "const",
            "AnswerB"   => "constants",
            "AnswerC"   => "define",
            "AnswerD"   => "def",
            "Correct"   => "AnswerC"
            ],
            ["question" => "Mặc định của một biến không có giá trị được thể hiện với từ khóa",
            "AnswerA"   => "none",
            "AnswerB"   => "null",
            "AnswerC"   => "undef",
            "AnswerD"   => "Không có khái niệm như vậy trong PHP",
            "Correct"   => "AnswerB"
            ],
            ["question" => "PHP tượng trưng cho cái gì:",
            "AnswerA"   => "Preprocessed Hypertext Page",
            "AnswerB"   => "Hypertext Transfer Protocol",
            "AnswerC"   => "PHP: Hypertext Preprocessor",
            "AnswerD"   => "Hypertext Markup Language",
            "Correct"   => "AnswerC"
            ],
            ["question" => "Ký hiệu nào dùng để kết thúc câu lệnh trong PHP?",
            "AnswerA"   => "dấu chấm đôi ( :: )",
            "AnswerB"   => "dấu phẩy ( , )",
            "AnswerC"   => "dấu chấm phẩy ( ; )",
            "AnswerD"   => "dấu chấm than ( ! )",
            "Correct"   => "AnswerC"
            ],
        ];
        if (!empty($_POST['nextPage'])) {
            for ($i = 0; $i < 5; $i++) {
                $temp = isset($_POST[$i]) ? $_POST[$i] : '';
                setcookie($i, $temp, 0, "/");
            }
            header("Location: trang2.php");
        }
        
    ?>
<body onload="deleteCookie(0,10)">
    <div class="quiz">
    <div class = "question">
    <h2>Trắc nghiệm PHP</h2>
    </div>
    <form action="" method="post">
    <?php
        for ($i = 0; $i < 5; $i++) {
        ?>
        <div class = "question">
            <h3><?php echo "Câu hỏi ".($i+1).": ".$_SESSION['questions'][$i]["question"]?></h3>
            <div>
                <label><input type="radio" id="choice1" name=<?php echo $i?> value="AnswerA" 
                >
                <?php echo "A. ".$_SESSION['questions'][$i]["AnswerA"]?>
                </label>
            </div>
            <div>
                <label><input type="radio" id="choice2" name=<?php echo $i?> value="AnswerB" 
                >
                <?php echo "B. ".$_SESSION['questions'][$i]["AnswerB"]?>
                </label>
            </div>
            <div>
                <label><input type="radio" id="choice3" name=<?php echo $i?> value="AnswerC" 
                >
                <?php echo "C. ".$_SESSION['questions'][$i]["AnswerC"]?></label>
            </div>
            <div>
                <label><input type="radio" id="choice4" name=<?php echo $i?> value="AnswerD"
                >
                <?php echo "D. ".$_SESSION['questions'][$i]["AnswerD"]?></label>
            </div> 
        </div>
        <?php
            }
        ?>
     <div class = "next">
    <input type="submit" value="Next" name="nextPage" class="button1"/>
    </div>
    </form>
    </div>
   </div>
</body>

</html>