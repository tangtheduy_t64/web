<!DOCTYPE html>
<html>

<head>
    <title>Trắc nghiệm</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="trang2.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="script.js"></script>
</head>
<?php
        session_start();
        if (!empty($_POST['submit'])) {
            for ($i = 5; $i < 10; $i++) {
                $temp = isset($_POST[$i]) ? $_POST[$i] : '';
                setcookie($i, $temp, 0, "/");
            }
            header("Location: trang3.php");
        }
    ?>
<body onload="deleteCookie(5,10)">
    <div class="quiz">
    <div class = "question">
    <h2>Trắc nghiệm PHP</h2>
    </div>
    <form action="" method="post">
    <?php
        for ($i = 5; $i < 10; $i++) {
        ?>
        <div class = "question">
            <h3><?php echo "Câu hỏi ".($i+1).": ".$_SESSION['questions'][$i]["question"]?></h3>
            <div>
                <label><input type="radio" id="choice1" name=<?php echo $i?> value="AnswerA" 
                >
                <?php echo "A. ".$_SESSION['questions'][$i]["AnswerA"]?>
                </label>
            </div>
            <div>
                <label><input type="radio" id="choice2" name=<?php echo $i?> value="AnswerB" 
                >
                <?php echo "B. ".$_SESSION['questions'][$i]["AnswerB"]?>
                </label>
            </div>
            <div>
                <label><input type="radio" id="choice3" name=<?php echo $i?> value="AnswerC" 
                >
                <?php echo "C. ".$_SESSION['questions'][$i]["AnswerC"]?></label>
            </div>
            <div>
                <label><input type="radio" id="choice4" name=<?php echo $i?> value="AnswerD"
                >
                <?php echo "D. ".$_SESSION['questions'][$i]["AnswerD"]?></label>
            </div>
        </div>
        <?php
            }
        ?>
    <div class = "next">
    <input type="submit" value="Submit" name="submit" class="button1"/>
    </div>
    </form>
    </div>
   </div>
</body>

</html>