<!DOCTYPE html>
<html>

<head>
    <title>Danh sách sinh viên</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="index.css" />
</head>

<body>
    <div class="form">
        <?php
        session_start();
        $select = ["" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu"];
        $khoa_search = isset($_SESSION['khoa_search']) ? $_SESSION['khoa_search'] : '';
        $tukhoa_search = isset($_SESSION['tukhoa_search']) ? $_SESSION['tukhoa_search'] : '';
        $count = "XXX";
        if (!empty($_POST['submit'])) {
            $khoa_search = isset($_POST['khoa_search']) ? $_POST['khoa_search'] : '';
            $tukhoa_search = isset($_POST['tukhoa_search']) ? $_POST['tukhoa_search'] : '';
            $_SESSION["khoa_search"] = $khoa_search;
            $_SESSION["tukhoa_search"] = $tukhoa_search;
        }
        ?>
        <form method="post" action="index.php" enctype="multipart/form-data">
            <div class="row">
                <div class="label">Khoa</div>
                <div class="column"></div>
                <select name="khoa_search" id="" value="<?php echo $khoa_search; ?>">
                    <?php
                    foreach ($select as $key => $value) {
                    ?>
                        <option <?php
                                if ($khoa_search == $key) {
                                    echo 'selected';
                                } ?> value="<?php echo $key; ?>"><?php echo $value ?></option>
                    <?php
                    }
                    ?>
                </select>

            </div>
            <div class="row">
                <div class="label">Từ khóa</div>
                <div class="column"></div>
                <input class="input" type="text" name="tukhoa_search" value="<?php echo $tukhoa_search; ?>">
            </div>
            <button id="delete" class = "delete" onClick = "delete_search()">Xóa</button>
            <input class="button" type="submit" value="Tìm kiếm" name="submit">
        </form>
        <script>
            function delete_search() {
                <?php
                    $khoa_search = "";
                    $tukhoa_search = "";
                    $_SESSION["khoa_search"] = "";
                    $_SESSION["tukhoa_search"] = "";
                ?>
            }         
        </script> 
        <div class="count">Số sinh viên tìm thấy: <?php echo $count; ?> </div>
        <!-- <form method="post" action="index.php" enctype="multipart/form-data">
            <input class="button1" type="submit" value="Thêm" name="addStudent">
        </form> -->
        
        <a href="form.php" class="button1" style="text-decoration:none;">Thêm</a>
        <table style="width:100%;font-size:20px" >
            <tr>
                <th width = "5%">No</th>
                <th width = "20%">Tên sinh viên </th>
                <th width = "50%">Khoa </th>
                <th>Action</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td><button class="edit" >Xóa</button>
                <button class="edit" >Sửa</button></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Trần Thị B</td>
                <td>Khoa học máy tính</td>
                <td><button class="edit" >Xóa</button>
                <button class="edit" >Sửa</button></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Nguyễn Hoàng C</td>
                <td>Khoa học vật liệu</td>
                <td><button class="edit" >Xóa</button>
                <button class="edit" >Sửa</button></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Đinh Quang D</td>
                <td>Khoa học vật liệu</td>
                <td><button class="edit" >Xóa</button>
                <button class="edit" >Sửa</button></td>
            </tr>
        </table>
    </div>
</body>

</html>
