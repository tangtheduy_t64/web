<!DOCTYPE html>
<html>

<head>
    <title>Đăng ký</title>
    <meta charset="UTF-8">
    <style>
        .form {
            border: solid 2px;
            border-color: #0B59AC;
            margin-top: 80px;
            margin-left: 400px;
            margin-right: 800px;
            padding-left: 60px;
            padding-right: 60px;
            padding-top: 30px;
            padding-bottom: 10px;
        }

        .row {
            display: flex;
            margin-left: 10px;
            margin-right: 10px;
        }

        .row_first {
            display: flex;
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 80px;
        }

        .label {
            width: 25%;
            background-color: #6495ED;
            border: solid 2px;
            border-color: #0B59AC;
            color: white;
            font-size: 20px;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
            text-align: center;
        }

        .label_gender {

            color: black;
            font-size: 20px;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 15px;
            margin-bottom: 10px;
            text-align: center;
        }

        .column {
            width: 5%;
        }

        .input {
            width: 60%;
            border: solid 2px;
            border-color: #0B59AC;
            padding: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-top: 10px;
            margin-bottom: 10px;

        }

        .button {
            width: 20%;
            background-color: #59AC0B;
            border: solid 2px;
            border-color: #0B59AC;
            color: white;
            font-size: 20px;
            padding: 10px;
            padding-top: 18px;
            padding-bottom: 18px;
            border-radius: 10px;
            text-align: center;
            margin-left: 220px;
            margin-top: 30px;
            margin-bottom: 70px;
        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin-top: 25px;
        }
        select{
            border: solid 2px;
            border-color: #0B59AC;
            width: 200px;
            height: 48px;
            margin-top: 10px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="form">
        <div class="row_first">
            <div class="label">Họ và tên</div>
            <div class="column"></div>
            <input class="input">
        </div>
        <div class="row">
            <div class="label">Giới tính</div>
            <div class="column"></div>
            <?php
            $gender = ["Nam", "Nữ"];
            for ($i = 0; $i < count($gender); $i++) {
            ?>
                <input class="gender" type="radio" name="gender" value="<?php echo $i; ?>">
                <div class="label_gender"><?php echo $gender[$i] ?></div>
            <?php
            }
            ?>

        </div>
        <div class="row">
            <div class="label">Phân khoa</div>
            <div class="column"></div>
            <select name="" id="">
                <option value=""></option>
                <?php
                $select = ["MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu"];
                foreach( $select as $key => $value){
                ?>
                <option value="<?php echo $key; ?>"><?php echo $value?></option>
                <?php
            }
            ?>
            </select>
        </div>
        <div class="button">Đăng ký</div>
    </div>
</body>

</html>
