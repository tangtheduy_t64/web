<!DOCTYPE html>
<html>

<head>
    <title>Danh sách sinh viên</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="index.css" />
</head>

<body>
    <div class="form">
        <?php
        include("connection.php");
        $sql = "SELECT id,name,faculty,address
        FROM student 
        ";
        $result = $conn->query($sql);
        $select = ["" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu"];
        $genders = ["Nam", "Nữ"];
        $khoa_search = "";
        $tukhoa_search = "";
        $count = "XXX";
        if (!empty($_POST['submit'])) {
            $khoa_search = isset($_POST['khoa_search']) ? $_POST['khoa_search'] : '';
            $tukhoa_search = isset($_POST['tukhoa_search']) ? $_POST['tukhoa_search'] : '';
            $_SESSION["khoa_search"] = $khoa_search;
            $_SESSION["tukhoa_search"] = $tukhoa_search;
            $search = "SELECT id,name,faculty,address
            FROM student
            WHERE faculty like '%$khoa_search%' and (name like '%$tukhoa_search%' or address like '%$tukhoa_search%') 
            ";
            $result = $conn->query($search);
        }
        ?>
        <form method="post" action="index.php" enctype="multipart/form-data">
            <div class="row">
                <div class="label">Khoa</div>
                <div class="column"></div>
                <select name="khoa_search" id="" value="<?php echo $khoa_search; ?>">
                    <?php
                    foreach ($select as $key => $value) {
                    ?>
                        <option <?php
                                if ($khoa_search == $key) {
                                    echo 'selected';
                                } ?> value="<?php echo $key; ?>"><?php echo $value ?></option>
                    <?php
                    }
                    ?>
                </select>

            </div>
            <div class="row">
                <div class="label">Từ khóa</div>
                <div class="column"></div>
                <input class="input" type="text" name="tukhoa_search" value="<?php echo $tukhoa_search; ?>">
            </div>
            <button id="delete" class = "delete" onClick = "delete_search()">Xóa</button>
            <input class="button" type="submit" value="Tìm kiếm" name="submit">
        </form>
        <script>
            function delete_search() {
                <?php
                    $khoa_search = "";
                    $tukhoa_search = "";
                    $_SESSION["khoa_search"] = "";
                    $_SESSION["tukhoa_search"] = "";
                ?>
            }         
        </script> 
        <div class="count">Số sinh viên tìm thấy: <?php echo $result->num_rows; ?> </div>

        <a href="form.php" class="button1" style="text-decoration:none;">Thêm</a>
        
        <table style="width:100%;font-size:20px" >
            <tr>
                <th width = "5%">No</th>
                <th width = "20%">Tên sinh viên </th>
                <th width = "20%">Khoa</th>
                <th width = "30%">Địa chỉ</th>
                <th>Action</th>
            </tr>
            <?php
                 if ($result->num_rows > 0) {
                    // output data of each row
                    $count = 1;
                    while($row = $result->fetch_array()) {

            ?>
                      <tr>
                        <td><?php  echo $count; ?></td>
                        <td><?php echo $row["name"]?></td>
                        <td><?php echo $select[$row["faculty"]]?></td>
                        <td><?php echo $row["address"]?></td>
                        <td><button class="edit" type="submit" name = "delete" value=<?php echo $row["id"]?> >Xóa</button>
                        <button class="edit" >Sửa</button></td>
                    </tr>
            <?php        
                        $count++;
                    }  
                  } 
            ?>        
            
        </table>
    </div>
</body>

</html>