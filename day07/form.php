<!DOCTYPE html>
<html>

<head>
    <title>Đăng ký</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="form.css" />
</head>

<body>
    <div class="form">
        <?php
        $name = "";
        $gender = "";
        $phankhoa = "";
        $birth = "";
        $address = "";
        if (!empty($_POST['submit'])) {
            $check = true;
            $name = isset($_POST['name']) ? $_POST['name'] : '';
            $gender = isset($_POST['gender']) ? $_POST['gender'] : '';
            $phankhoa = isset($_POST['phankhoa']) ? $_POST['phankhoa'] : '';
            $birth = isset($_POST['birth']) ? $_POST['birth'] : '';
            $address = isset($_POST['address']) ? $_POST['address'] : '';
            // $target_dir    = "uploads/";
            // $target_file   = $target_dir . basename($_FILES["file"]["name"]);
            if (empty($name)){
                echo '<div class ="alert">Hãy nhập tên.</div> ';
                $check =false;
            }
            if (strlen($gender) == 0){ # do empty(0) tra ve true
                echo '<div class ="alert">Hãy chọn giới tính.</div>';
                $check =false;
            }
            if (empty($phankhoa)){
                echo '<div class ="alert">Hãy chọn phân khoa.</div>';  
                $check =false;
            }
            if (empty($birth)){
                echo '<div class ="alert">Hãy nhập ngày sinh.</div>';
                $check =false;
            }
            #check type anh khi chon anh (ko bat buoc chon anh)
            if($_FILES['fileUpload']['name']!=''){
                $image_type = exif_imagetype($_FILES['fileUpload']["tmp_name"]);
                if (!$image_type) {
                    echo '<div class ="alert">Không chọn đúng định dạng ảnh.</div>';
                    $check =false;
                }
            }
            #html tự validate ngày -> chỉ check null
            if ($check){ #sang trang moi khi ko co loi
                session_start();
                $_SESSION["name"] = $name;
                $_SESSION["gender"] = $gender;
                $_SESSION["phankhoa"] = $phankhoa;
                $date=date_create($birth);
                $_SESSION["birth"] = date_format($date,"d/m/Y");
                $_SESSION["address"] = $address;
                if($_FILES['fileUpload']['name']!=''){  
                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    $time = date('YmdHis');
                    $namefile = pathinfo($_FILES['fileUpload']['name'],PATHINFO_FILENAME);
                    $ext = pathinfo($_FILES['fileUpload']['name'], PATHINFO_EXTENSION);
                    $namefile = $namefile.'_'.$time.'.'.$ext; #them time vao duong dan anh
                    $uploads_dir = 'upload/';
                    if(!is_dir($uploads_dir)){ #kiem tra folder ton tai
                        mkdir($uploads_dir,0777,true);
                    }
                    move_uploaded_file($_FILES['fileUpload']['tmp_name'], $uploads_dir.basename($namefile));
                    $_SESSION["file"]   = $uploads_dir.basename($namefile) ;
                }
                else{
                    $_SESSION["file"] = "";
                }
                 
                // if($_FILES['fileUpload']['name']!='')
                //     $_SESSION["file"] = file_get_contents($_FILES['fileUpload']['tmp_name']);
                // else{
                //     $_SESSION["file"] = "";
                // }
                header("Location: /day07/confirm.php");
            }   

        }
        ?>
        <form method="post" action="form.php" enctype="multipart/form-data">
            <div class="row_first">
                <div class="label">Họ và tên<span class="span">*</span></div>
                <div class="column"></div>
                <input class="input" type="text" name="name" value="<?php echo $name; ?>">
            </div>
            <div class="row">
                <div class="label">Giới tính<span class="span">*</span></div>
                <div class="column"></div>
                <?php
                $genders = ["Nam", "Nữ"];
                for ($i = 0; $i < count($genders); $i++) {
                ?>
                    <input class="gender" type="radio" name="gender" value="<?php echo $i; ?>" 
                    <?php  {if ($gender== $i) {echo 'checked';}
                    } ?>>
                    <div class="label_gender"><?php echo $genders[$i] ?></div>
                <?php
                }
                ?>

            </div>
            <div class="row">
                <div class="label">Phân khoa<span class="span">*</span></div>
                <div class="column"></div>
                <select name="phankhoa" id="" value="<?php echo $phankhoa; ?>">
                    <?php
                    $select = ["" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu"];
                    foreach ($select as $key => $value) {
                    ?>
                        <option <?php 
                                    if ($phankhoa == $key) {
                                        echo 'selected';
                                    } ?> value="<?php echo $key; ?>"><?php echo $value ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="row">
                <div class="label">Ngày sinh<span class="span">*</span></div>
                <div class="column"></div>
                <input class="birth" data-date-format="DD/MM/YYYY" type="date" name="birth" placeholder="dd/mm/yyyy"
                value = "<?php echo $birth; ?>"
                >
            </div>
            <div class="row">
                <div class="label">Địa chỉ</div>
                <div class="column"></div>
                <input class="input" type="text" name="address" value = "<?php echo $address; ?>">
            </div>
            <div class="row">
                <div class="label">Hình ảnh</div>
                <div class="column"></div>
                <input class="input1" type="file" name="fileUpload" accept="image/*">
            </div>
            <input class="button" type="submit" value="Đăng ký" name="submit">
        </form>
    </div>
</body>

</html>