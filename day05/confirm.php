<!DOCTYPE html>
<html>

<head>
    <title>Xác nhận</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="confirm.css" />
</head>

<body>
    <?php
        session_start();
        $name = $_SESSION["name"];
        $gender = $_SESSION["gender"];
        $phankhoa = $_SESSION["phankhoa"];
        $birth = $_SESSION["birth"];
        $address = $_SESSION["address"];
        #$file = $_SESSION["file"] ;
        $file = $_SESSION["file"];
        // echo $file;
        // echo file_get_contents($file);
        $genders = ["Nam", "Nữ"];

        $select = ["" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu"];
    ?>
    <div class="form">
        <form method="post" action="confirm.php" enctype="multipart/form-data">
            <div class="row_first">
                <div class="label">Họ và tên</div>
                <div class="column"></div>
                <!-- <p><?php echo $name;?></p> -->
                <input class="input" type="text" name="name"  readonly value="<?php echo $name; ?>">
            </div>
            <div class="row">
                <div class="label">Giới tính</div>
                <div class="column"></div>
                <!-- <p><?php echo $genders[$gender];?></p> -->
                <input class="input" type="text" name="gender"  readonly value="<?php echo $genders[$gender]; ?>" >
            </div>
            <div class="row">
                <div class="label">Phân khoa</div>
                <div class="column"></div>
                <input class="input" type="text" name="phankhoa"  readonly value="<?php echo $select[$phankhoa]; ?>">
            </div>
            <div class="row">
                <div class="label">Ngày sinh</div>
                <div class="column"></div>
                <input class="input" type="text" name="date" readonly value="<?php echo $birth; ?>">

            </div>
            <div class="row">
                <div class="label">Địa chỉ</div>
                <div class="column"></div>
                <input class="input" type="text" name="address"  readonly value="<?php echo $address; ?>">
            </div>
            <div class="row">
                <div class="label">Hình ảnh</div>
                <div class="column"></div>
                <img class="img" src="<?php echo $file; ?>" alt = "No image">
            </div>
            <input class="button" type="submit" value="Xác nhận" name="submit">
        </form>
    </div>
</body>

</html>